import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router';
// import VueMoment from 'vue-moment';
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
// don't forget to import CSS styles
import 'tiptap-vuetify/dist/main.css'
// Vuetify's CSS styles 
import 'vuetify/dist/vuetify.min.css'

import Admin from './components/MainAdmin.vue';
import MainWhithDevice from './components/MainAds.vue';
import Main from './components/Main.vue';

import ListAds from './components/adminComponents/ListAds.vue';
import ListDevice from './components/adminComponents/ListDevice.vue';
import ListGroup from './components/adminComponents/ListGroup.vue';
// const vuetifyForTiptap = new vuetify();

Vue.use(vuetify);
Vue.use(TiptapVuetifyPlugin, {
  // the next line is important! You need to provide the Vuetify Object to this place.
  vuetify, // same as "vuetify: vuetify"
  // optional, default to 'md' (default vuetify icons before v2.0.0)
  iconsGroup: 'mdi'
});
Vue.use(VueRouter);
Vue.use(require('vue-moment'));
// Vue.use(VueMoment);
var router = new VueRouter({
  routes: [
    { path: '/admin', component: Admin },//, meta: { requiresAuth: true},
    { path: '/admin/listAds', component: ListAds },
    { path: '/admin/listDevice', component: ListDevice },
    { path: '/admin/listGroup', component: ListGroup },
    { path: '/:idDevice', component: MainWhithDevice },
    { path: '/', component: Main }
  ]
})
Vue.config.productionTip = false;
fetch(process.env.BASE_URL + "config.json")
  .then((response) => {
    response.json().then((config) => {
      Vue.prototype.$config = config
      new Vue({
        vuetify,
        render: h => h(App),
        router: router,
      }).$mount('#app')
    })
  })
