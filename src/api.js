import axios from 'axios';


export default axios.create({
  baseURL: process.env.NODE_ENV === 'production'
  ? '/api/v1/'
  : 'http://localhost:8081/api/v1/',

  // baseURL: 'http://localhost:8081/api/v1/',

  timeout: 5000,
  headers: {
    'Content-Type': 'application/json'
    }
});